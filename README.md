# Bash shell functions, configuration and scripts

A collection of shell codes and shortcuts I've been keeping since about 2005. These come from a long history of loosely managed hacks and archives, mostly as a part of [dotfiles](https://gitlab.com/milohax-attic/dotfiles). There are utilities to aid in Unix hacking for my hobbies, and also for work at different places that I've been employed.

The aim of this project is to make the collection able to be loaded in a bash interactive session, separately managed from `$HOME` configurations (that is, separate to _dotfiles_ and server provisioning). To assist with this, there are some conventions.

* Private/helper functions are named `__funcname` (two underscores). These are not intended to be used interacively
* Completion functions are named `_funcname` (one underscore, same name as function being completed)
* Same conventions for variable names
* Minimal customization of `.bashrc` and `.bash_profile`
* Functions grouped into "modules", loaded from `00_modules.sh::__bootstrap_modules`
* Separation of Policy (my own preferences), from Mechanism (how the collection works)
* Configuration of policy quirks (directory paths) can be loaded from <sub>UNIX</sub> properties files. Also facilitates _DRY_ configuration with other shells

## Features

* [Self-documenting shell functions](https://milosophical.me/blog/2018/4-bits-part3.html), inspired by EMACS, and similar to Fish `--description` strings and `functions` listing

* Operating System and Linux flavour detection

* Modularised loading

* Can be loaded ("`source`d") without needing to change `.bashrc`, allowing alternate configurations. See `modules/20_env.sh::super` function

## Installing

Since this repository allows stand-alone use, the minimum installation is:

1. clone the repository (recommended location: `~/lib/bash`)
1. `source` the modules loader
1. bootstrap the modules

Here's an example snippet, which uses the the included `.bashrc`. One could append this to an existing `~/.bashrc`:

```bash
  BASHING_RC=${BASHING-~/lib/bash}/link/.bashrc
  test -f ${BASHING_RC} && source ${BASHING_RC}
```

(If you clone to a location other than `~/lib/bash`, then set `$BASHING` to your working copy path)

### Replacing existing bash dotfiles

Appending the above snippet to an existing `~/.bashrc` is all that's required. But if you would like to _replace_ your default bash configuration, then you can link in the bash dotfiles using the `link-bash.sh` script. This will make a backup of your existing config in the `backups` subdirectory, and then replace them with symlinks into files in the `link/` directory.

### Hacking and Debugging

The modules are loaded from the private `__bootstrap_modules` function. By default this prints nothing during loading, but display features can be turned on using these functions:

* `bashrc_banner` turns on a startup banner display
* `bashrc_boring` turns off all bashrc/module display features
* `bashrc_debug` Toggle bashrc module load debugging
* `bashrc_slow` make module loading slower
* `bashrc_fast` make module loading fast again
* `bashrc_pretty` turn on all bashrc/module display features

Also, each module may be sourced individually using the `src` function. If you are trying to track down slow module loads, this can help a lot.

Happy Bashing!
