#!/usr/bin/env bash
# Link the files from link/ into $HOME
# This effectively "installs" Bashing as your bash setup
# Existing files are backed up to backups/<basename>.<unix_time>

bashing_dir=${BASHING-~/lib/bash}
backups=${bashing_dir}/backups

shopt -s dotglob  # find hidden files
shopt -s nullglob # if no match, then expands to null instead of glob
for file in ${bashing_dir}/link/*; do
  base="$(basename ${file})"
  dest="${HOME}/${base}"

  if [[ -e ${dest} ]]; then
    echo -e " \033[1;34m➜\033[0m  Backing up ~/$base."
    # Create backup dir if it doesn't already exist.
    [[ -e ${backups} ]] || mkdir -p ${backups}
    # Backup file / link / whatever.
    mv ${dest} ${backups}/${base}.$(date +%s)
  fi
  echo -e " \033[1;32m✔\033[0m  Linking ~/${base}"
  ln -sf ${file} ${dest}
done
shopt -u dotglob
shopt -u nullglob
