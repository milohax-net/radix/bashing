[[ $(hostname) =~ ${GITLAB_WORKSTATION} ]] || return

function __cdpath_add(){
    [[ -d "${1}" ]] && export CDPATH="${CDPATH}:${1}"
}
function __add_cdpath(){
    [[ -d "${1}" ]] && export CDPATH="${1}:${CDPATH}"
}
__add_cdpath ${LAB}
__cdpath_add ${LAB}/gdk
__add_cdpath .
__cdpath_add ~

# web shortcuts

alias ohshitgit='open https://ohshitgit.com/'
alias devcheat='open https://about.gitlab.com/handbook/engineering/development/dev/create-static-site-editor/developer-cheatsheet/'

# Support tools
alias gh=greenhat
alias fs=fast-stats
alias glogs=gitlab-structlog

alias gl1password='pass -c gitlab/1password'

function _chk_search_args(){
  local func="${1}"; shift
  local descr="${1}"; shift
  local noun="${1}"; shift
  if test -z "${1}"; then
    error "${func}: Error: no ${noun} supplied"
    usage "${func} <${noun}>" ${descr}
    return 1
  fi
}

function zen(){
  local FUNCDESC="Open a Zendesk ticket"
  _chk_search_args ${FUNCNAME} ${FUNCDESC} ticket "${@}"

  open "https://gitlab.zendesk.com/agent/tickets/${1}"
}

function glhb(){
  local FUNCDESC="Search the GitLab Handbook"
  _chk_search_args ${FUNCNAME} ${FUNCDESC} search_terms "${@}"

  local terms="${@}"
  open "https://about.gitlab.com/handbook/#stq=${terms}"
}


function gld(){
  local FUNCDESC="Search the GitLab Documentation"
  _chk_search_args ${FUNCNAME} ${FUNCDESC} search_terms "${@}"

  open "https://docs.gitlab.com/search/?q=${@}"
}


# Local gitlab in docker
# https://docs.gitlab.com/omnibus/docker/
export GITLAB_DOCKER_BASE=${GITLAB_DOCKER_BASE-~/srv/docker-gitlab}

function gitlab-docker() {
  sudo docker run --detach \
  --hostname gitlab.docker.local \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --volume $GITLAB_DOCKER_BASE/config:/etc/gitlab \
  --volume $GITLAB_DOCKER_BASE/logs:/var/log/gitlab \
  --volume $GITLAB_DOCKER_BASE/data:/var/opt/gitlab \
  gitlab/gitlab-ee:latest
}

function zdwipe() {
  local FUNCDESC="Clean old files from Zendesk Downloads"
  # See https://gitlab.com/gitlab-com/support/toolbox/zd-dl-wiper/
  # and https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/

  DL=~/Downloads/zen
  A=_zd_before_wipe.txt
  B=_zd_wiped.txt

  /bin/ls "$DL" > "$DL/$A"

  ruby ~/lab/zd-dl-wiper/zd-dl-wiper.rb

  /bin/ls "$DL"/ > "$DL/$B"
  diff "$DL/$A" "$DL/$B"
}

# Added by GDK bootstrap
export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig:${PKG_CONFIG_PATH}"

# Added by GDK bootstrap
export RUBY_CONFIGURE_OPTS="--with-openssl-dir=/usr/local/opt/openssl@1.1 --with-readline-dir=/usr/local/opt/readline"

# Source Support Toolbox aliases
load ~/lab/gitlab.com/gitlab-com/support/toolbox/dotfiles/aliases/*

# Print gitlab access tokens from 1Password
# Note: these are aliases: I don't want them in every shell environment!
#       To add to a variable:  export GITLAB_API_PRIVATE_TOKEN=$(glpat)
alias glpat="op item get 'glpat-mlockhart@gitlab.com' --field=password"
alias glpat-glab="op item get 'glpat-mlockhart@gitlab.com-glab' --field=token"
alias glabpat=glpat-glab
