GCP_SDK_DIR=/usr/local/google-cloud-sdk/latest/google-cloud-sdk
is_osx && GCP_SDK_DIR=/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk

is_tty && load ${GCP_SDK_DIR}/path.bash.inc ${GCP_SDK_DIR}/completion.bash.inc

# GCloud aliases
alias gci='gcloud compute instances'
alias gsh='gcloud compute ssh'
alias gauth='gcloud auth activate-service-account --key-file='
alias gacct='gcloud config set account'
alias gaddress='gcloud compute addresses'
alias gaddr='gcloud compute addresses'
