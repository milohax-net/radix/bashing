is_tty || exit 0 # only need this for interactive shells
[[ -e /opt/anaconda3 ]] || return 1 # and only if anaconda exists

# Conda 3 init, moved from .bashrc to here.
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
  eval "$__conda_setup"
else
  if [ -f "/opt/anaconda3/etc/profile.d/conda.sh" ]; then
    . "/opt/anaconda3/etc/profile.d/conda.sh"
  else
    export PATH="/opt/anaconda3/bin:$PATH"
  fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda deactivate

function cex() {
  FUNCDESC="Execute Conda-managed command."

  if [[ ! ${CONDA_PREFIX} ]]; then
    conda activate
    __cex_set=1
  fi
  command ${@}
  if [[ $__cex_set ]]; then
    conda deactivate
  fi
  unset __cex_set
}
