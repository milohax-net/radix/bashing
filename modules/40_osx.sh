# OSX-only stuff. Abort if not OSX.
is_osx || return 1

# APPLE, Y U PUT /usr/bin B4 /usr/local/bin?!
path_remove /usr/local/bin
path_add /usr/local/bin PREPEND

#MJL20170204 Homebrew PATH fixes
path_add /usr/local/sbin
path_add /usr/local/opt/openssl/bin
path_add /usr/local/opt/grep/libexec/gnubin

#Find the SDK and add it to the CPATH for compiling, see 
# https://github.com/python-pillow/Pillow/issues/3438#issuecomment-543812237
export CPATH=$(xcrun --show-sdk-path)/usr/include

# Trim new lines and copy to clipboard
alias c="tr -d '\n' | pbcopy"

# Make 'less' more.
[[ "$(type -P lesspipe.sh)" ]] && eval "$(lesspipe.sh)"

# Start ScreenSaver. This will lock the screen if locking is enabled.
alias ss="open /System/Library/Frameworks/ScreenSaver.framework/Versions/A/Resources/ScreenSaverEngine.app"

#MJL20190210 - squash the CPU-hungry Google Drive File System
alias gdfs='cpulimit -l 2 -p $(pgrep -f "crash_handler_token=")&'
#MJL20191012 - another one: Apple's ReportCrash help runs a lot after Catalina
alias nocrash='cpulimit -l 1 -p $(pgrep -f "ReportCrash")&'

