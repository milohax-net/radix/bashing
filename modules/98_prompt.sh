# only need this for interactive shells
is_tty || return 0

# Use Starship, unless missing, or disabled with environment
if [[ -z $STARSHIP_DISABLED ]]; then
  if is_exe starship; then
      eval "$(starship init bash)"
      export STARSHIP_CONFIG=~/etc/starship/bash.toml
  fi
fi

## fallback to a basic Bash prompt with colour and a nicer prompt character

[[ $TERM_PROGRAM == vscode ]] && return 0 # preserve the VSCode prompt

if ! is_exe starship || [[ -n $STARSHIP_DISABLED ]]; then
    [[ "${USER}" == "root" ]] || [[ "${UID}" == "0" ]] && __USER=root
    [[ -z ${SSH_TTY} ]] && [[ -z ${WINDOW} ]] && __TERM=smart

  if [[ ${__TERM} == smart  ]]; then
    if [[ "${__USER}" == "root" ]]; then
      __ENDING="Ω"
    else if is_raspbian; then
        __ENDING="π"
      else
        __ENDING="β"
      fi
    fi
  else
    if [[ "${__USER}" == "root" ]]; then
      __ENDING="#"
    else
      __ENDING="$"
    fi
  fi

  echo "${C9}${__ENDING} "
fi

function prompt_amstrad() {
  local FUNCDESC="Toggle using a AMSTRAD Ready prompt. If an argument is supplied, force to Amstrad"
  if [[ -z ${__USE_AMSTRAD_PROMPT} || -n "${1}" ]]; then
    export __USE_AMSTRAD_PROMPT=1
  else
    unset __USE_AMSTRAD_PROMPT
  fi
}

#MJL20170205 toggle using a simple prompt
# If an argument is supplied, force it to simple
function prompt_simple() {
  local FUNCDESC="Toggle using a simple prompt. If an argument is supplied, force to simple"
  if [[ -z ${__USE_SIMPLE_PROMPT} || -n "${1}" ]]; then
    export __USE_SIMPLE_PROMPT=1
  else
    unset __USE_SIMPLE_PROMPT
  fi
}
alias simple_prompt=prompt_simple
alias awesome_prompt=prompt_simple

#MJL20180402 toggle using monster prompt
# If an argument is supplied, force it to simple
function prompt_monster() {
  local FUNCDESC="Toggle using a monster prompt. If an argument is supplied, force to simple"
  if [[ -z ${__USE_MONSTER_PROMPT} || -n "${1}" ]]; then
    export __USE_MONSTER_PROMPT=1
  else
    unset __USE_MONSTER_PROMPT
  fi
}
alias monster_prompt=prompt_monster

#MJL20170207 toggle command history trace
# sometimes this can be useful
function prompt_trace() {
  local FUNCDESC="Toggle command history trace. Sometimes this can be helpful."
  if [[ -z ${__USE_TRACE_PROMPT} || -n "${1}" ]]; then
    export __USE_TRACE_PROMPT=1
  else
    unset __USE_TRACE_PROMPT
  fi
}
alias trace_prompt=prompt_trace


function prompt_reset(){
  local FUNCDESC="Reset the shell prompt to standard (not simple, noot other"
  unset __USE_SIMPLE_PROMPT __USE_AMSTRAD_PROMPT
  unset __USE_MONSTER_PROMPT __USE_TRACE_PROMPT
}

# Maintain a per-execution call stack.
__PROMPT_STACK=()
trap '__PROMPT_STACK=("${__PROMPT_STACK[@]}" "${BASH_COMMAND}")' DEBUG

#TODO add $SHLVL if greater than 0

function __prompt_command() {
  local EXIT_CODE=${?}
  PS1=""
  # If the first command in the stack is prompt_command, no command was run.
  # Set exit_code to 0 and reset the stack.
  [[ "${__PROMPT_STACK[0]}" == "__PROMPT_COMMAND" ]] && EXIT_CODE=0
  __PROMPT_STACK=()

  # Manually load z here, after $? is checked, to keep $? from being clobbered.
  [[ "$(type -t _z)" ]] && _z --add "$(pwd -P 2>/dev/null)" 2>/dev/null

  #MJL20170207 disable the awesome prompt for basic environments
  [[ -n ${ANDROID_ROOT} ]] && prompt_simple 1

  # While the simple_prompt environment var is set, disable the awesome prompt.
  [[ "$__USE_SIMPLE_PROMPT" ]] && PS1='[\u@\h:\w]\$ ' && return

  # While the simple_prompt environment var is set, disable the awesome prompt.
  [[ "$__USE_AMSTRAD_PROMPT" ]] && PS1='\033[0;33mReady\n' && return


  # Setup local $c0-$c9 color vars.
  PROMPT_COLORS[9]=;
  local i; for i in ${!PROMPT_COLORS[@]}; do
    local C${i}="\[\e[0;${PROMPT_COLORS[${i}]}m\]"
  done

  if [[ -n ${MC_SID} ]]; then
    #MJL20170205 single-line prompt for Midnight Commander
    PS1="$(__prompt_titlebar "MC - ${USER}@${HOSTNAME%%.*}")"
    #flags: screen venv
    PS1="${PS1}$(__prompt_screen)$(__prompt_venv)"
    #path: [user@host:path]
    PS1="${PS1}${C1}[${C0}\u${C1}@${C0}\h${C1}:${C0}\w${C1}]${C9}"
    #codes: (jobs)exitcode
    PS1="${PS1}$(__prompt_jobs)$(__prompt_exitcode "${EXIT_CODE}")"
    PS1="${PS1}$(__prompt_ending)"
  else
    #MJL20170207 Cowboy's Awesome prompt is the fall-through case
    # http://twitter.com/cowboy/status/150254030654939137
    #MJL20170204 titlebar: [dir] - user@host:/full/working/dir
    PS1="${PS1}$(__prompt_titlebar "[${HOSTNAME%%.*}:$(basename ${PWD})] - ${USER}@${HOSTNAME%%.*}:${PWD}")"
    # path: [user@host:path]
    PS1="${PS1}${C1}[${C0}\u${C1}@${C0}\h${C1}:${C0}\w${C1}]${C9}"
    if [[ -n ${__USE_MONSTER_PROMPT} ]]; then
      PS1="${PS1}$(__prompt_sizes)"
    fi
    if [[ ! ${PWD} =~ ${SSHFS_MOUNT_POINT} ]]; then
      # svn: [repo:lastchanged]
      PS1="${PS1}$(__prompt_svn)"
      # git: [branch:flags]
      PS1="${PS1}$(__prompt_git)"
      # hg:  [branch:flags]
      PS1="${PS1}$(__prompt_hg)"
    fi
  fi
}
