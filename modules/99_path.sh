# Fix the shell path. Goes last of all because $PATH is set all over the place.
# I use this to stick in personal/local paths too


function dedupe_path() {
  local FUNCDESC='Removes duplicates from $PATH variable.
Call for side-effects, no parameters taken.'
  # see http://unix.stackexchange.com/questions/40749/remove-duplicate-path-entries-with-awk-command

  local oldPATH X
  if [ -n "${PATH}" ]; then
    old_PATH="${PATH}:"; PATH=
    while [ -n "${old_PATH}" ]; do
      X=${old_PATH%%:*}       # the first remaining entry
      case ${PATH}: in
        *:"${X}":*) ;;         # already there
        *) PATH=${PATH}:${X} ;;    # not there yet
      esac
      old_PATH=${old_PATH#*:}
    done
    PATH=${PATH#:}
  fi
}
alias path_dedupe=dedupe_path

# MJL20190514 I want these entries in specific places on the PATH
path_remove ~/bin
path_remove ~/Work/bin
path_remove .

path_add ~/bin PREPEND
for D in \
  ${MISE_DATA_DIR}/shims \
  ~/bin \
  ~/Work/bin \
  ~/.rd/bin \
  ${CARGO_HOME}/bin \
  ${GOPATH}/bin \
  ${BASHING}/bin \
  /usr/local/bin \
  /usr/local/sbin \

do
  path_add ${D}
done
is_osx && is_arm && path_add /opt/homebrew/bin PREPEND
if [[ $(hostname) = ${GITLAB_WORKSTATION} ]]; then
  path_add ${GITLAB_LAB_BIN}
  path_add ~/Library/Application\ Support/cloud-code/installer/google-cloud-sdk/bin/
  path_add ~/lab/gitlab.com/gitlab-com/support/toolbox/dotfiles/bin
fi
path_add .

dedupe_path
export PATH
alias path='echo ${PATH}'
