#!/usr/bin/env bash
# Wrapper for gitmux to change some strings in ways that it doesn't 
# allow for. Also replace 'origin' with a symbol for the git host.

WORKING_COPY=${1}
[[ -d ${WORKING_COPY} ]] || exit 1
pushd $WORKING_COPY &> /dev/null
  STATUS=$(git status) &> /dev/null || exit 2
  URL=$(git config --get remote.origin.url) &> /dev/null
  if [[ "${STATUS}" =~ "No commits yet" ]] ; then
    BRANCH="[no commits yet]"
  else
    BRANCH=$(echo ${STATUS} | awk '/On branch/ {print $3}')
  fi
  case ${URL} in
    *"bitbucket"*) GITHOST="" ;;
    *"github"*)    GITHOST="" ;;
    *"lab"*)       GITHOST="" ;;
    *)             GITHOST="" ;;
  esac
popd &> /dev/null
gitmux -cfg ~/.gitmux.conf ${WORKING_COPY} \
  | sed "s/origin/${GITHOST}/" \
  | sed "s/${BRANCH}//2" \
  | tr -d '/'
