#!/usr/bin/env bash

# Create home base directories
#
# The actual pathnames come from environment variables, which should
# have been set before running this script. My Dotfiles project sets the
# ones that I like, in .config/env/places.env ($ETC/env/places.env)

if [[ ${HAX} ]]; then #Assume they're all set if $HAX is
  case " ${WORKSTATIONS} " in
    *" $(hostname) "*)
      for d in \
        ${BAK} ${BIN} ${FUN} \
        ${HAX} ${IMG} ${KEY} \
        ${LAB} ${LIB} ${NET} \
        ${SRV} ${TMP} ${VMS} ; do
          [[ -e ${d} ]] || mkdir -p ${d}
      done
      ;;
  *)
    for d in \
      ${BAK} ${BIN} \
      ${HAX} ${LAB} \
      ${LIB} ${TMP} ; do
        [[ -e ${d} ]] || mkdir -p ${d}
    done
    ;;
  esac
  [[ -e ~/var ]] || ln -s ${VAR} ~/var
  [[ -e ~/etc ]] || ln -s ${CONFIG} ~/etc
else
  echo "Home directory places not defined. Source places.env file" 1>&2
  exit 1
fi
