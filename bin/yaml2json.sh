#!/usr/bin/env bash --norc --noprofile

if [ -z "$1" ]; then
    echo "Usage: $0 <yaml_file>"
    exit 1
fi

yaml_file="$1"

# Check if jq is installed
if ! command -v jq &> /dev/null; then
    echo "jq is not installed. Please install jq before running this script."
    exit 1
fi

# Convert YAML to JSON using jq
json_data=$(cat "$yaml_file" | jq -nR --argjson yaml "$(cat)" '$yaml | (.|split("\n")[:-1]|map(select(length > 0))|map(split(": "))|map({"key": .[0], "value": .[1:]|join(": ")})|map({(.key): .value})|add)')

# Print the resulting JSON data
echo "$json_data"
