#!/usr/bin/env bash
# Pull/push dotfiles, bashing, and fishing repos

for REPO in ~/lib/bash ~/lib/fish ~/hax/bootstrap; do
  pushd ${REPO} &> /dev/null || continue
  echo -e "Repo:\t$(pwd)"
  echo -en "Pull:\t"
  git pull --no-rebase
  echo -en "Push:\t"
  git push
  popd &> /dev/null
  echo
done

if is_exe yadm; then
  echo -en "Pull:\t"
  yadm pull --no-rebase
  echo -en "Push:\t"
  yadm push
fi
