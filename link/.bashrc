# Bash Run Commands
# Run by login or interactive shells, and also by Bash when called as /bin/sh in
# some situations. So this needs to be POSIX syntax or guard Bashisms

# have we been here before? (SUSE /etc/profile will source $HOME/.bashrc)
type -p __bootstrap_modules && return

# Source system global definitions
test -f /etc/bashrc && source /etc/bashrc

# Unless in POSIX mode, load the rest of the Bashing modules into the
# environment. (POSIX will fail)
if kill -l|grep SIG &> /dev/null; then #is not POSIX?
  # Where Bashing is installed
  export BASHING=${BASHING-~/lib/bash}
  export BASH_MODULES=${BASHING}/modules

  source ${BASH_MODULES}/00_modules.sh
  __bootstrap_modules
fi
