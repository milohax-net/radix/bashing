find ${TEMP} -ctime +15 -delete 2> /dev/null
find ${HOME}/Downloads/zen/ -type d -mtime +30 \
  -not \( -path '*/\.git*' -o -path '*/\.vscode*' \) \
  -exec rm -rf {} + &> /dev/null
find ${HOME}/Uploads -ctime +29 -delete 2> /dev/null
find ${HOME}/bak -ctime +183 -delete 2> /dev/null
clear
